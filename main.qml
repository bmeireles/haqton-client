/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15

import solutions.qmob.haqton 1.0

import "FontAwesome"

ApplicationWindow {
    width: 380
    height: width * 16/9
    visible: true
    title: qsTr("HaQton")

    FontLoader { id: gameFont; source: "assets/fonts/PocketMonk-15ze.ttf" }
    Audio { source: "assets/audio/Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    QtObject {
        id: internal
        property int margins: 10
    }
    
    Rectangle {
        id: mainRect
        anchors.fill: parent
        color: "#44a8e4"

        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "#44a8e4"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#f79154"; duration: 5000 }
            ColorAnimation { to: "#f3d034"; duration: 5000 } ColorAnimation { to: "#8bc740"; duration: 5000 }
        }
    }

    StackView {
        anchors { fill: parent; margins: internal.margins }
        initialItem: Item {
            ColumnLayout {
                width: parent.width
                anchors { verticalCenter: parent.verticalCenter; verticalCenterOffset: -haqtonLabel.height }
                Label {
                    id: haqtonLabel
                    Layout.topMargin: 8*internal.margins
                    font { family: gameFont.name; pixelSize: 64 }
                    Layout.alignment: Qt.AlignHCenter
                    color: "white"; text: "HaQton!"
                    SequentialAnimation on scale {
                        loops: Animation.Infinite
                        PropertyAnimation { to: 0.25; duration: 1500; easing.type: Easing.OutElastic }
                        PropertyAnimation { to: 1; duration: 1500; easing.type: Easing.OutElastic }
                        PauseAnimation { duration: 10000 }
                    }
                    SequentialAnimation on rotation {
                        loops: Animation.Infinite
                        PropertyAnimation { to: -45; duration: 1000; easing.type: Easing.OutElastic }
                        PropertyAnimation { to: 45; duration: 1000; easing.type: Easing.OutElastic }
                        PropertyAnimation { to: 0; duration: 1000; easing.type: Easing.OutElastic }
                        PauseAnimation { duration: 10000 }
                    }
                }
                GridLayout {
                    columns: 2; columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
                    Layout.fillWidth: true; Layout.fillHeight: false
                    Repeater {
                        model: [
                            {
                                icon: Icons.faPlusCircle,
                                text: "Criar uma partida"
                            },
                            {
                                icon: Icons.faGamepad,
                                text: "Participar de uma partida"
                            },
                            {
                                icon: Icons.faDiceD20,
                                text: "Sobre o HaQton"
                            },
                            {
                                icon: Icons.faMedal,
                                text: "Patrocinadores"
                            }
                        ]
                        GameButton {
                            Layout.fillWidth: true; Layout.fillHeight: true
                            iconLabel { text: modelData.icon; color: mainRect.color }
                            text: modelData.text
                        }
                    }
                }
            }
        }
    }
}
